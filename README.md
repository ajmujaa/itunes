# Itunes

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development environment

* Browser: Google Chrome
* Angular version: 5.2
* Styles: SASS/SCSS

## Completed tasks
* Create a microsite that displays music albums, and a detail view for each album
* Consume the iTunes API to get a list of albums to display, eg: (https://itunes.apple.com/search?term=Beatles&entity=album)
* In case you need additional API calls, have a look at the API docs at [https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/](https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/)
* Your layout should be responsive. Lay out items in a grid for desktop (see `itunes-desktop.png`), and in a list for smaller displays (see `itunes-mobile.png`).
* Upon clicking on an album, its details should be displayed. 
* In the details view, at least album cover, title, and track list should be included. Feel free to add additional information as you like.
* Make the list of albums sortable and filterable by title (client side).
* When you are in the details view of an album and hit "refresh" you should see the same details as before.
* Angulare Animations is being used
* Roboto webfont is being used

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
