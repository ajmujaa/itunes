import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { APP_INITIALIZER } from '@angular/core';
import { AppConfig } from './app-config';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SearchService } from './common/search.service';

import { AppComponent } from './app.component';
import { AlbumsListComponent } from './albums-list/albums-list.component';
import { AppRoutingModule } from './app-routing.module';
import { AlbumComponent } from './album/album.component';

import { OrderModule } from 'ngx-order-pipe';

export function initConfig(config: AppConfig) {
  return () => config.search();
}

@NgModule({
  declarations: [
    AppComponent,
    AlbumsListComponent,
    AlbumComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    OrderModule
  ],
  providers: [
    AppConfig,
    { provide: APP_INITIALIZER, useFactory: initConfig, deps: [AppConfig], multi: true},
    SearchService,
    HttpModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
