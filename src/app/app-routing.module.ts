import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'; 

import { AlbumComponent } from "./album/album.component";
import { AlbumsListComponent } from "./albums-list/albums-list.component";


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: AlbumsListComponent},
  {path: 'album/:id', component: AlbumComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
