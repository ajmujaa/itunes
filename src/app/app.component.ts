import { Component, OnInit } from '@angular/core';
import { SearchService } from './common/search.service';

import { FadeInAnimation, SearchBarAnimation } from './common/animations/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    SearchBarAnimation,
    FadeInAnimation
  ]
})
export class AppComponent implements OnInit {
  title = 'app';
  private defaultAlbums: any;
  searchText: string = '';
  state: string = 'small';

  constructor(private searchService: SearchService) { 
  }

  ngOnInit() {
    this.getAlbums('2018');
  }

  public animate(){
    this.state = (this.state === 'small' ? 'large' : 'small');
  }

  public getAlbums(searchTerm: string): any {
    this.searchService.search(searchTerm, 'album').subscribe(res => { this.defaultAlbums = res});
    this.searchText = '';
  }
}

