import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchService } from './../common/search.service';
import { FadeInAnimation } from './../common/animations/animations';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss'],
  animations: [FadeInAnimation]
})
export class AlbumComponent implements OnInit {

  private albumId: number;
  private album: any;
  private tracks: any;

  constructor(private route: ActivatedRoute,
              private albumService: SearchService) {
    this.albumId = +this.route.snapshot.url[1].path;
   
  }

  ngOnInit() {
    this.getAlbumDetails(this.albumId);
  }

  private getAlbumDetails(albumId: any): any {
    this.albumService.getAlbum(albumId, 'album')
    .subscribe(res => {
      this.album = res[0]; 
      this.getTracksForAlbum(this.album.collectionName)
    });
  }

  private getTracksForAlbum(albumName: string): any { 
    this.albumService.getAlbum(albumName, 'musicTrack')
    .subscribe(res => { 
      this.tracks = res;
    });
  }

}
