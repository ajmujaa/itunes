export class ServiceLocator {
	
	private baseUrl: string;

	constructor(baseUrl: string){
		this.baseUrl = baseUrl;
	}

	public searchUrl(term: string, entity: string): string { 
		return `${this.baseUrl}` + 'term=' + `${term}` + '&entity=' + `${entity}`; 
	}

}
