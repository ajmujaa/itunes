import { Injectable} from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';
import { ServiceLocator } from './service-locator';

interface ConfigData {
	endPointUrl: string;
}

@Injectable()
export class AppConfig {
	private config: ConfigData;
	public service: ServiceLocator;

	constructor(private http: Http) {}

	public search() {
		return new Promise((resolve, reject) => {
			let request: any = null;
			request = this.http.get(environment.endPointUrl);
			request
				.map(res => res.json())
				.catch((error: any) => {
					resolve(error);
				})
				.subscribe((responseData) => {
					this.config = responseData.config as ConfigData;
					const endPointUrl = this.config.endPointUrl;
					this.service = new ServiceLocator(endPointUrl);
					resolve(true);
				})
		})
	}
}
