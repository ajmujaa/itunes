import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SearchService } from './../common/search.service';
import { Subscription } from 'rxjs/Subscription';
import { FadeInAnimation } from './../common/animations/animations';

@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.scss'],
  animations: [FadeInAnimation]
})
export class AlbumsListComponent implements OnInit, OnDestroy {

  private albums: any;
  private albumsCount: number;
  subscription: Subscription;
  order: string = 'collectionName';
  orders = ['Ascending', 'Descending'];
  selectedOrder = 'Ascending';
  reverse: boolean = false;

  constructor(private albumsListService: SearchService) {
  }

  ngOnInit() {
    this.getAlbumsList();
  }

  public setOrder(sortby: string): any{
    this.selectedOrder = sortby;
    if(sortby === 'Ascending'){
      this.reverse = false;
    }else{
      this.reverse = true;
    }
  }

  public getAlbumsList(): void{
    this.albumsListService.search('2018', 'album');
    this.subscription = this.albumsListService.getResults()
      .subscribe(res => { 
        this.albums = res; 
        this.albumsCount = this.albums.length;
      });
    
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
