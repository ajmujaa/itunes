import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

export const SearchBarAnimation = [
    trigger('searchBarAnimation', [
        state('small', style({
            width: '100%',
            marginLeft: '0'
        })),
        state('large', style({
            width: '200%',
            marginLeft: '-100%'
        })),
        transition('small <=> large', animate('100ms ease-in'))
    ])
]