import { Injectable } from '@angular/core';
import { AppConfig } from '../app-config';
import { Http, HttpModule, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class SearchService {

  private broadcast = new ReplaySubject<any>(0);

  constructor(private http: Http,
    private appConfig: AppConfig) { }

  public search(searchTerm: any, entity: string): Observable<any> {
    return this.http.get(this.appConfig.service.searchUrl(searchTerm, entity))
      .map((res: Response) => {
        if(res){
          this.broadcast.next(res.json().results);
        }
      })
      .catch(this.handleError);
  }

  public getAlbum(album: string, entity: string): Observable<any> {
    return this.http.get(this.appConfig.service.searchUrl(album, entity))
      .map((res: Response) => res.json().results)
      .catch(this.handleError);
  }

  public getResults(): Observable<any> {
    return this.broadcast.asObservable();
  }

  public clearResults(){
    this.broadcast.next([]);
  }

  private handleError(error: any) {
    return Observable.throw(error.message || error);
  }  

}
